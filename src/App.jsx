import React, {Component} from 'react';

// Routes
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import Routes from './routes/routes';

// Browser history
const browserHistory = createBrowserHistory();

export default class App extends Component {
  render() {
    return (
          <Router history={browserHistory}>
            <Routes />
          </Router>
    );
  }
}
