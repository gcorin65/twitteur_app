import React , { Component } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom';

//Component
import LogIn from '../views/LogIn';
import Feed from '../views/Feed/Index';

 
export default class Routes extends Component {
    render() {
      return (
        <Switch>
          <Redirect
            exact
            from="/"
            to="/connection"
          />
          <Route
            component={LogIn}
            exact
            path="/connection"
          />
          <Route
            component={Feed}
            exact
            path="/news"
          />
          <Redirect to="/not-found" />
        </Switch>
      );
    }
  }
  