import React from 'react';
import styled from 'styled-components'
import { makeStyles } from '@material-ui/core/styles';
// Image
import Avatar from '@material-ui/core/Avatar';
// Component
import Background from './images/feature.jpeg';
import { RegularText, MediumGreyText, SpanGreyText } from '../../../Texts/Texts';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
// Icon
import CalendarTodayIcon from '@material-ui/icons/CalendarToday'
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder'
import MailIcon from '@material-ui/icons/Mail'
import ModeCommentIcon from '@material-ui/icons/ModeComment'
import CachedIcon from '@material-ui/icons/Cached'
import CallMadeIcon from '@material-ui/icons/CallMade'
import { Container, ListItem, Box, Grid, IconButton, List, ListSubheader, ListItemText, Button, Divider, TextField } from '@material-ui/core';
// Media
import Breakfast from './images/breakfast.jpg'
import Burger from './images/burgers.jpg'
import Camera from './images/camera.jpg'
import Morning from './images/morning.jpg'
import Hats from './images/hats.jpg'
import Honey from './images/honey.jpg'
import Vegetables from './images/vegetables.jpg'
import Plants from './images/plant.jpg'
import Star from './images/star.jpg'
// Axios
import axios from 'axios';


export const ContainerFLuidCustom = styled.div`
    background-color: #e6ecf0;
`;
export const ContainerPrimaryCustom = styled.div`
    display: grid;
    grid-template-columns: 2fr 1fr;
    grid-template-rows: 1fr;
    margin-top: 4rem;
`;
export const LeftContent = styled.div`
    display: flex;
    flex-direction: column;
    margin-top: 20px;
`;
export const RightContent = styled.div`

`

export const  HeroProfil = styled.div`
    height: 200px;
    background-image: url(${Background});
`
export const EditButton = styled.button`
    max-height: 40px;
    color: #1da1f2;
    border-color: #1da1f2;
    border: 1px solid rgba(29, 161, 242, 0.5);
    padding: 0 1em;
    border-radius: 100px;
    margin: 1em;
    &:hover {
        background-color: rgba(29, 161, 242, 0.08);
    }
`
export const ContainerEditButton = styled.div`
    display:flex;
    text-align:right;
    justify-content:space-between;
    background-color: white;
`
export const InfoProfilContainer = styled.div`
    padding: 8px 8px 1rem 8px;
    background-color: white;
`

export function Avatars() {
    const classes = useStyles();
    return (
        <Avatar alt="Thor" src={require("./images/thor.jpg")} className={classes.bigAvatar} />
    );
}

export const useStyles = makeStyles(theme => ({
    avatar: {
        margin: 10,
        width:100,
        height:100
    },
    bigAvatar: {
        margin: 10,
        width: 120,
        height: 120,
        marginTop:-70,
        marginBottom:15,
        border:'4px solid white'
    },
    tabs: {
        backgroundColor: theme.palette.common.white,
    },
    tweetContainer:{
        height: '16rem',
        backgroundColor: theme.palette.common.white
    },
    tweet: {
        padding: '1rem 10px',
        flexDirection: 'column',
        alignItems: 'flex-start',
    },
    avatarTweet:{
        display: 'flex',
        alignItems:'center'
    },
    success:{
        color:'#17bf63'
    },
    danger:{
        color: '#e0245e',
        fill: '#e0245e'
    },
    gridMedia: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridListMedia: {
        height: '14rem',
    },
    according:{
        backgroundColor: theme.palette.common.white,
        marginTop:10
    },
    suggestions:{
        backgroundColor: theme.palette.common.white,
        marginTop: 20,
        marginLeft:10
    },
}));


export  class Tweet extends React.Component{
    state = {
        messages: [],
        message:'',
        name:''
        }
    componentDidMount() {
        axios.get(`http://touiteur.cefim-formation.org/list`)
            .then(res => {
            this.setState((prevState) => ({ messages: res.data.messages }));
        })
    }

    handleChangeMessage = (e) => {
        this.setState({
            message: e.target.value

        })
    }

    handleChangeName = (e) => {
        this.setState({
            name: e.target.value

        })
    }

    handleSubmit = (e) => {
        e.preventDefault()
            const message = this.state.message
            const name = this.state.name
        axios.post(`http://touiteur.cefim-formation.org/send`, `&name=${name}&message=${message}`)
    }

    render() {
    return (
    <React.Fragment>
    <form onSubmit={this.handleSubmit} autoComplete="on" style={{display:'flex', justifyContent:'space-around'}}>
      <TextField
        id="standard-name"
        label="Name"
        margin="normal"
        onChange={this.handleChangeName}
      />
      <TextField
        id="standard-name"
        label="Message"
        margin="normal"
        onChange={this.handleChangeMessage}
      />
      <Button type="submit">Publish</Button>
      </form>
    {this.state.messages.reverse().slice(0, 10).map(({  name, message, ts, like, id }) => (
    <ListItem button key={id}>
        <Grid container spacing={6} wrap="nowrap">
        <Grid item style={{display: 'flex',alignItems:'center'}}>
            <Avatar
            style={{
                margin: 10,
                width:100,
                height:100
            }}
            alt="Avatar"
            src={require("./images/ironman.jpg")}
            />
        </Grid>
        <Grid item>
            <Grid container spacing={2}>
            <Grid item xs={12}>
                <Typography bold inline>{name}</Typography>{' '}
                <Typography light inline>@{name}</Typography>{' '}
                <Typography light inline></Typography>
                <Typography>{message}</Typography>
            </Grid>
            <Grid item xs={12}>
                <Box ml="-12px" display="inline-flex" alignItems="center">
                <IconButton>
                    <ModeCommentIcon/>
                </IconButton>
                <Typography light inline>0</Typography>
                </Box>
                <Box ml="32px" display="inline-flex" alignItems="center">
                <IconButton style={{color:'#17bf63'}}>
                    <CachedIcon/>
                </IconButton>
                <Typography style={{color:'#17bf63'}}>0</Typography>
                </Box>
                <Box ml="32px" display="inline-flex" alignItems="center">
                <IconButton style={{color: '#e0245e'}}><FavoriteBorderIcon/></IconButton>
                <Typography style={{color: '#e0245e'}} >{like}</Typography>
                </Box>
                <Box ml="32px" display="inline-flex" alignItems="center">
                <IconButton><MailIcon/></IconButton>
                </Box>
            </Grid>
            </Grid>
        </Grid>
        </Grid>
    </ListItem>
    ))}
    </React.Fragment>
    );
}}

export const tileData = [
    {
        img: Breakfast,
        title: 'Breakfast',
        author: 'author',
        cols: 2,
    },
    {
        img: Burger,
        title: 'Burger',
        author: 'me',
        cols: 1,
    },
    {
        img: Morning,
        title: 'Morning',
        author: 'me',
        cols: 1,
    },
    {
        img: Honey,
        title: 'Honey',
        author: 'me',
        cols: 1,
    },
    {
        img: Hats,
        title: 'Hat',
        author: 'me',
        cols: 1,
    },
    {
        img: Camera,
        title: 'Camera',
        author: 'me',
        cols: 1,
    },
    {
        img: Vegetables,
        title: 'Vegetables',
        author: 'me',
        cols: 2,
    },
    {
        img: Star,
        title: 'Star',
        author: 'me',
        cols: 2,
    },
    {
        img: Plants,
        title: 'Plants',
        author: 'me',
        cols: 1,
    },
];

export function ImageGridList() {
    const classes = useStyles();

    return (
        <div className={classes.gridMedia}>
        <GridList cellHeight={160} className={classes.gridListMedia} cols={3}>
            {tileData.map(tile => (
            <GridListTile key={tile.img} cols={tile.cols || 1}>
                <img src={tile.img} alt={tile.title} />
            </GridListTile>
            ))}
        </GridList>
        </div>
    );
}

const userList = [
    {
        image: 'https://randomuser.me/api/portraits/women/1.jpg',
        primary: 'Never stop thinking',
        secondary: '@never_stop',
    },
    {
        image: 'https://randomuser.me/api/portraits/men/1.jpg',
        primary: 'React Geek',
        secondary: '@react',
    },
    {
        image: 'https://randomuser.me/api/portraits/women/2.jpg',
        primary: 'Thailand',
        secondary: '@wonderful_th',
    },
];

export function AccordingWhom() {
    const classes = useStyles();
    return (
      <List subheader={<ListSubheader>According to whom</ListSubheader>} className={classes.according}>
        {userList.map(({ image, primary, secondary }) => (
          <React.Fragment key={primary}>
            <ListItem button>
              <Avatar alt="Avatar" link src={image} />
              <ListItemText primary={primary} secondary={secondary} />
              <Button variant="outlined" color="primary">
                Follow
              </Button>
            </ListItem>
            <Divider/>
          </React.Fragment>
        ))}
        <ListItem button>
          <ListItemText>
            <Typography link>Show More</Typography>
          </ListItemText>
        </ListItem>
      </List>
    );
  }
  

function TabContainer({ children, dir }) {
        return (
          <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
            {children}
            </Typography>
        );
}

TabContainer.propTypes = {
        children: PropTypes.node.isRequired,
        dir: PropTypes.string.isRequired,
};

export  function FullWidthTabs() {
    const classes = useStyles();
    const theme = useTheme();
    const [value, setValue] = React.useState(0);

    function handleChange(event, newValue) {
        setValue(newValue);
    }
    function handleChangeIndex(index) {
        setValue(index);
    }
    return (
        <React.Fragment>
        <AppBar position='sticky' className={classes.tabs}>
            <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
        >
            <Tab label="Tweets" />
            <Tab label="Tweets and reponses" />
            <Tab label="Media" />
            <Tab label="Liking"/>
            </Tabs>
        </AppBar>
        <SwipeableViews
            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
            index={value}
            onChangeIndex={handleChangeIndex}
            className={classes.tweetContainer}
        >
        <TabContainer dir={theme.direction}><Tweet/></TabContainer>
        <TabContainer dir={theme.direction}>Item Two</TabContainer>
        <TabContainer dir={theme.direction} style={{height: '14rem'}}><ImageGridList/></TabContainer>
        <TabContainer dir={theme.direction}>Item Four</TabContainer>
        </SwipeableViews>
    </React.Fragment>
    );
}


export function HeaderProfile(){
    return(
        <React.Fragment>
            <HeroProfil/>
            <ContainerEditButton>
                <Avatars/>
            <EditButton> Editer profil</EditButton>
            </ContainerEditButton>
            <InfoProfilContainer>
                <RegularText isBold={true} isLeft={true}>ThorToto</RegularText>
                <MediumGreyText >@imFatiFato</MediumGreyText>
                <MediumGreyText><CalendarTodayIcon/> Inscrit depuis le {Date()}</MediumGreyText>
                <RegularText isBold={false} isLeft={true}>4068 <SpanGreyText>followers</SpanGreyText>   3 <SpanGreyText>following</SpanGreyText></RegularText>
            </InfoProfilContainer>
            <FullWidthTabs/>
        </React.Fragment>
    )
}

const suggestions = [
  {
    image: 'https://randomuser.me/api/portraits/men/3.jpg',
    primary: 'Yeoman',
    secondary: '@whatsup yeo',
  },
  {
    image: 'https://randomuser.me/api/portraits/women/3.jpg',
    primary: 'GGWP',
    secondary: '@goodgamewellplay',
  },
  {
    image: 'https://randomuser.me/api/portraits/men/4.jpg',
    primary: 'Sawasdee',
    secondary: '@helloTH',
  },
];

export function TrackWho() {
    const classes = useStyles()
return (
    <List subheader={<ListSubheader>Who to follow</ListSubheader>} className={classes.suggestions}>
        {suggestions.map(({ image, primary, secondary }) => (
        <React.Fragment key={primary}>
            <ListItem button>
            <Avatar alt="Avatar" link src={image} />
            <ListItemText primary={primary} secondary={secondary} />
            <Button variant="outlined" color="primary">
                Follow
            </Button>
            </ListItem>
            <Divider />
        </React.Fragment>
        ))}
        <ListItem button>
        <ListItemText>
            <Typography link>Show More</Typography>
        </ListItemText>
        </ListItem>
    </List>
    );
}

export class PopularNow extends React.Component {
    state ={
        mostPopular: [],
        trending: []
    }

    componentDidMount(){
        axios.get(`http://touiteur.cefim-formation.org/trending`)
            .then(res => {
            this.setState((prevState) => ({ mostPopular: res.data }));
            const popular = this.state.mostPopular
            let ntable = Object.entries(popular).sort((a, b) => b[1] - a[1]);
            let mostPopular = ntable.slice(0,3)
            console.log(`les mots 3 mots les plus utilisés sont ${mostPopular}`)
            this.setState({mostPopular: [
                {index: "0", word: `${mostPopular[0][0]}`},
                {index: "1", word: `${mostPopular[1][0]}`},
                {index: "2", word: `${mostPopular[2][0]}`},
             ] }) 
            this.setState({trending: this.state.mostPopular})
        })
    }

    render(){
        console.log(this.state.trending);
        return (
            <List subheader={<ListSubheader>Trending Words</ListSubheader>} style={{backgroundColor: 'white',
                marginTop: 20,
                marginLeft:10}}>
            {this.state.trending.map(({ index, word }) => (
                <React.Fragment key={index}>
                <ListItem button>
                    <ListItemText>
                    <Typography primary>#{word}</Typography>
                    </ListItemText>
                </ListItem>
                {/* <Divider /> */}
                </React.Fragment>
            ))}
            {/* <ListItem button>
                <ListItemText>
                <Typography link>Show More</Typography>
                </ListItemText>
            </ListItem> */}
            </List>
        );
    }
}

export default function Fedd(){
        return (
            <React.Fragment>
                <ContainerFLuidCustom>
                    <Container>
                        <ContainerPrimaryCustom>
                            <LeftContent>
                                <HeaderProfile/>
                                <AccordingWhom/>
                            </LeftContent>
                            <RightContent>
                                <TrackWho/>
                                <PopularNow/>
                            </RightContent>
                        </ContainerPrimaryCustom>
                    </Container>
                </ContainerFLuidCustom>
            </React.Fragment>
        )
    }


