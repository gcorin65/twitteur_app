import React, { Component } from 'react'
// Style
import '../../App.css'
//Custom Components
import PrimarySearchAppBar from './components/Header'
import Feed from './components/PrimaryContent';


export default class App extends Component {
  render() {
    return (
        <React.Fragment>
            <PrimarySearchAppBar/>
            <Feed/>
        </React.Fragment>
    )
  }
}
