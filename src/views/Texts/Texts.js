import styled from '@emotion/styled/macro';
// import {theme} from "../../ui/theme";

// Grey Text (Main Color)
export const LargeGreyText = styled('p')({
    fontSize : '16px',
    fontWeight :'700',
    fontFamily : 'Poppins, sans-serif',
    color : '#657786',
    margin : 0,
})

export const MediumGreyText = styled('p')(({isBold})=>({
    fontFamily : 'Poppins, sans-serif',
    color : '#657786',
    fontSize: '14px',
    fontWeight: isBold ? '700' : '400',
}))

export const RegularGreyText = styled('p')(({isBold, isMaj, isCentered})=>({
    fontFamily : 'Poppins, sans-serif',
    color : '#657786',
    fontSize : '12px',
    fontWeight: isBold ? '700' : '400',
    textTransform : isMaj ? 'uppercase' : 'lowercase',
    margin : 0,
    textAlign: isCentered ? 'center' : 'initial'
}))

export const SpanGreyText = styled('span')({
    fontFamily : 'Poppins, sans-serif',
    color : '#657786',
    fontSize: '12px',
    margin : 0,
})

// Commentary Text(Grey)
export const CommentaryRegular = styled(MediumGreyText)({
    textTransform : 'none'
})

export const CommentarySpan = styled('span')(({isBold})=>({
    fontFamily : 'Poppins, sans-serif',
    color : '#657786',
    fontSize: '14px',
    fontWeight: isBold ? '700' : '400',
    margin : 0,
}))

// Main Text (Black)
export const MediumText = styled('p')({
    fontFamily : 'Poppins, sans-serif',
    fontSize : '14px',
    fontWeight: '400',
    color : 'black',
    margin : 0,
})

export const RegularText = styled('p')(({isBold, isMaj, isLeft, isPadding})=>({
    fontFamily : 'Poppins, sans-serif',
    fontSize : '16px',
    fontWeight: isBold ? '700' : '400',
    textTransform : isMaj ? 'uppercase' : 'none',
    color : 'black',
    margin : 0,
    textAlign : isLeft ? 'left' : 'right',
    padding : isPadding ? '0 0 0 15px' : '0 15px 0 0 '
}))

export const SmallText = styled('p')({
    fontSize : '10px',
    fontFamily : 'Poppins, sans-serif',
    color : 'black',
    margin : 0,
})

//Others
export const MediumDiscreetText = styled(MediumText)({
    color : '#657786',
    margin : 0,
})



// Text for Notifications

export const ErrorMessage = styled('p')({
    fontFamily : 'Poppins, sans-serif',
    fontSize : '16px',
    fontWeight : '600',
    color : 'red',
    textAlign : 'center',
    display : 'flex',
    flexDirection: 'column',
    justifyContent : 'center',
    alignItems : 'center',

});
